/* 
   \\\\\\\\\\\\\\\\\\\\    4.DISPLAY    ////////////////////
*/
static const unsigned char PROGMEM bleLogo [] = {
  0b00000000, 0b00000000, //                 
  0b00000001, 0b10000000, //        ##       
//  0b00000001, 0b11000000, //        ###      
  0b00000001, 0b01100000, //        # ##     
  0b00001001, 0b00110000, //     #  #  ##    
  0b00001101, 0b00110000, //     ## #  ##    
  0b00000111, 0b01100000, //      ### ##     
//  0b00000011, 0b11000000, //       ####      
  0b00000001, 0b10000000, //        ##       
//  0b00000011, 0b11000000, //       ####      
  0b00000111, 0b01100000, //      ### ##     
  0b00001101, 0b00110000, //     ## #  ##    
  0b00001001, 0b00110000, //     #  #  ##    
  0b00000001, 0b01100000, //        # ##     
//  0b00000001, 0b11000000, //        ###      
  0b00000001, 0b10000000, //        ##       
};

const unsigned char PROGMEM bootLogo [] = {
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x3F, 0x1F, 0x1F, 0x80, 0x00, 0x00, 0x00, 0x00, 0x30, 0x30, 0x98, 0xC0, 0x00, 0x00, 0x00, 0x00,
0x30, 0x30, 0x18, 0xCD, 0x9E, 0x1E, 0x3C, 0x78, 0x30, 0x38, 0x18, 0xCF, 0xB3, 0x30, 0x60, 0xCC,
0x3F, 0x1F, 0x18, 0xCC, 0x33, 0x30, 0x60, 0xCC, 0x30, 0x03, 0x9F, 0x8C, 0x3F, 0x1C, 0x38, 0xCC,
0x30, 0x01, 0x98, 0x0C, 0x30, 0x06, 0x0C, 0xCC, 0x30, 0x21, 0x98, 0x0C, 0x31, 0x06, 0x0C, 0xCC,
0x3F, 0x1F, 0x18, 0x0C, 0x1E, 0x3C, 0x78, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x80, 0x00, 0x00,
0x00, 0x00, 0xF8, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x01, 0x84, 0x00, 0x01, 0x80, 0x00, 0x00,
0x00, 0x01, 0x80, 0x78, 0xF1, 0x8F, 0x00, 0x00, 0x00, 0x01, 0xC0, 0xC4, 0x19, 0x99, 0x80, 0x00,
0x00, 0x00, 0xF8, 0xC0, 0xF9, 0x99, 0x80, 0x00, 0x00, 0x00, 0x1C, 0xC1, 0x99, 0x9F, 0x80, 0x00,
0x00, 0x00, 0x0C, 0xC1, 0x99, 0x98, 0x00, 0x00, 0x00, 0x01, 0x0C, 0xC5, 0x99, 0x98, 0x80, 0x00,
0x00, 0x00, 0xF8, 0x78, 0xF9, 0x8F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

uint32_t lastDisplayUpdate = 0;

//colors in uint32_t
//if you want to find your own: Serial.print("salmon ");Serial.println(convertRGBtoRGB8888( 128,128,128 ));
//white 4294967295 ,black 4278190080, yellow 4294967040, blue 4278190335, red 4294901760
//salmon 4294606962, crimson 4292613180, orangered 4294919424, orange 4294944000, peachpuff 4294957753
//khaki 4293977740, lime 4278255360, green 4278222848, seagreen 4281240407, cyan 4278255615, teal 4278222976
//navy 4278190208, magenta 4294902015, pink 4294951115, gray 4286611584, chocolate 4291979550
//brown 4289014314, maroon 4286578688 

//In our display, we use the following uint32_t variables (read/write from/to settings.h)
//colorMain ,colorTop ,colorBottom ,colorMainBg ,colorTopBg ,colorBottomBg 

//we need to keep the last text size we used because if that changes we should fill the background manually
uint8_t displayTopSectionLastTextSize = 255;
uint8_t displayMainSectionLastTextSize = 255;
uint8_t displayBottomSectionLastTextSize = 255;

//display can be divides (up) to 3 sections. Top,main,bottom
//if you set any height to 0, this segment will be ommited.
uint8_t displayTopSectionHeight = 20;
uint8_t displayMainSectionHeight = 24;
uint8_t displayBottomSectionHeight = 20;


////////// 4.2.POWER ON/OFF //////////
void clearDisplay() {  
  #ifdef SSD1306
    display.clearDisplay(); 
    display.display();
  #endif
  #ifdef SSD1331
    display.clearScreen();
  #endif
  #ifdef ST7735
    display.fillScreen(convertRGBAtoRGB565(COLOR_BLACK));
  #endif  
  #ifdef LEDSEGMENT  
    lc.clearDisplay(LED_ADDR);
  #endif  
}


void dimDisplay() {
  #ifdef ST7735
    ledcWrite(1, DISPLAY_PWM_DIM_BRIGHTNESS); // dimming           
  #endif         
  #ifdef LEDSEGMENT
    lc.setIntensity(LED_ADDR,LED_BRIGHTNESS_LOW);        
  #endif
}

void brightenDisplay() {
  #ifdef ST7735
    ledcWrite(1, displayMaxBr);   
  #endif         
  #ifdef LEDSEGMENT
    if (displayMaxBr > 15) {
      displayMaxBr = 15;
    }
    lc.setIntensity(LED_ADDR,displayMaxBr);        
  #endif
}


void wakeUpDisplay() {
  //re initialize a display after shutDownDisplay();
  #ifdef SSD1306
    display.ssd1306_command(SSD1306_DISPLAYON);
  #endif
  #ifdef SSD1331
    display.changeMode(NORMAL);
  #endif
  #ifdef ST7735
    ledcAttachPin(DISPLAY_BACKLIGHT_PIN, 1); //Dynamic backlight control , simply call ledcWrite(1,byte);
    ledcSetup(1, 100, 8);
    ledcWrite(1, displayMaxBr);    
  #endif
  #ifdef LEDSEGMENT
    #ifdef LUNAR_ECLIPSE_5
      digitalWrite(LED_LDO_EN_PIN,LED_LDO_ENABLE);  
    #endif
    if (displayMaxBr > 15) {
      displayMaxBr = 15;
    }
    lc.setIntensity(LED_ADDR,displayMaxBr);   
  #endif
  clearDisplay();
}

void shutDownDisplay() {
  #ifdef LEDSEGMENT
    lc.clearDisplay(LED_ADDR);  
    lc.setChar(LED_ADDR,0,'-',false);
    lc.setChar(LED_ADDR,2,'-',false);
    lc.setChar(LED_ADDR,4,'-',false);
    //briefly pause so the user can see we got his command
    delay(1000);
    lc.clearDisplay(LED_ADDR);
    #ifdef LUNAR_ECLIPSE_5
      digitalWrite(LED_LDO_EN_PIN,LED_LDO_DISABLE);  
    #endif
  #endif
  #ifdef SSD1306
    clearDisplay();
    display.ssd1306_command(SSD1306_DISPLAYOFF);
  #endif
  #ifdef SSD1331
    clearDisplay();
    display.changeMode(PWRSAVE);
  #endif
  #ifdef ST7735
    clearDisplay();
    ledcWrite(1, 0);
    //detach pwm
    ledcDetachPin(DISPLAY_BACKLIGHT_PIN); 
    delay(100); 
    pinMode(DISPLAY_BACKLIGHT_PIN, OUTPUT);
    delay(100);
    digitalWrite(DISPLAY_BACKLIGHT_PIN,0); //shut down backlight completely
    delay(100);
  #endif  
}



////////// 4.3.LED SEGMENT OUTPUT //////////
//Function responsible of printing the final weight in our LED segments
//It will automatically remove decimals if we run out of space
#ifdef LEDSEGMENT
  void writeTo7Seg(double number, uint8_t noOfDigits, uint8_t noOfDecimal) {  
      lc.clearDisplay(LED_ADDR);      
      char buffer[noOfDigits * 2]; //just to be safe
      dtostrf (number, noOfDigits, noOfDecimal, buffer);
      //uint8_t startat = 0;   
      uint8_t digitNo = 0;
      uint8_t dp = false;  
      //dtostrf considers the dot a separate byte
      //but for us (led segment) is just a flag on the previous byte
      //one way to solve this issue is to offset the counter. So, with only 1 for loop and an extra if, we are good.
      //If you prefer, you can loop the uint8_t array once and construct a second uint8_t array and a true/false flag for the dp
      int startcount=0;
      if (noOfDecimal == 0) {
        startcount=1;
      }      
      for (int i = startcount; i < noOfDigits * 2; i++) {
        if (i < noOfDigits-1 && buffer[i+1] == '.') {
          dp = true;
        }
        if (buffer[i] != '.') {
          lc.setChar(LED_ADDR,digitNo,buffer[i],dp);
          if (dp) {
            dp = false;
          }
          digitNo++;
        }
        if (digitNo == noOfDigits) {
          break;
        }
      }
  }
#endif



////////// 4.4.OLED/TFT OUTPUT //////////
#ifndef LEDSEGMENT
  void setRotation(uint8_t rotation) {  
    #ifndef LEDSEGMENT
      redrawDisplay = true;
      display.setRotation(rotation);  
    #endif
  }
  
  uint16_t convertRGBAtoRGB565(uint32_t color8888) {
    //convert uint32_t to R,G,B
    uint8_t R = 0;
    uint8_t G = 0;
    uint8_t B = 0;
  
    #ifdef DISPLAY_BINARY_COLOR
      //if black return 0, else 1
      return color8888 == 4278190080 ? 0 : 1;  
    #endif
  
    //ignore transparency
    R = (color8888 >> 16) & 0xFF;
    G = (color8888 >> 8) & 0xFF;
    B = (color8888) & 0xFF;
    if (DISPLAY_INVERT_RB) {
      return ((B & 0b11111000)<<8) + ((G & 0b11111100)<<3)+(R>>3);        
    }
    return ((R & 0b11111000)<<8) + ((G & 0b11111100)<<3)+(B>>3);
  }
  
  uint32_t convertRGBtoRGBA(uint8_t R, uint8_t G, uint8_t B) {
    uint32_t color888; 
    color888 = 0xFF;
    color888 <<= 8;
    color888 |= R;  
  //  color888 = R;
    color888 <<= 8;
    color888 |= G;
    color888 <<= 8;
    color888 |= B;  
    return color888;
  }
  
  void calculateTextSize (uint8_t leftColTextLength, uint8_t rightColTextLength, uint8_t colDivider, uint8_t textMaxSize, uint8_t sectionHeight, uint8_t &textSize, uint8_t &maxCols, uint8_t &xOffset) {
    if ((leftColTextLength <= DISPLAY_COLS_TS4/colDivider) && (rightColTextLength <= DISPLAY_COLS_TS4/colDivider) && textMaxSize > 3 && (DISPLAY_FONT_HEIGHT_TS1*4 <= sectionHeight)) {
      textSize=4;
      maxCols=DISPLAY_COLS_TS4;
      xOffset+=DISPLAY_XOFFSET_TS4;
    } else if ((leftColTextLength <= DISPLAY_COLS_TS3/colDivider) && (rightColTextLength <= DISPLAY_COLS_TS3/colDivider) && textMaxSize > 2 && (DISPLAY_FONT_HEIGHT_TS1*3 <= sectionHeight)) {
      textSize=3;
      maxCols=DISPLAY_COLS_TS3;
      xOffset+=DISPLAY_XOFFSET_TS3;    
    } else if ((leftColTextLength <= DISPLAY_COLS_TS2/colDivider) && (rightColTextLength <= DISPLAY_COLS_TS2/colDivider) && textMaxSize > 1 && (DISPLAY_FONT_HEIGHT_TS1*2 <= sectionHeight)) {
      textSize=2;
      maxCols=DISPLAY_COLS_TS2;
      xOffset+=DISPLAY_XOFFSET_TS2;
    } else {
      textSize=1;
      maxCols=DISPLAY_COLS_TS1;
      xOffset+=DISPLAY_XOFFSET_TS1;
    }
  }
  
  
  void updateDisplay() {
    #if defined(SSD1306)
      display.display();
    #endif
  }
  
  //very usefull little function to draw to the display
  //It can handle all displays and each section (top/bottom/middle) can have one or two "columns". It will handle text size automatically (1 or 2)
  //It cannot handle double row sections
  //Single column => leave rightColText empty
  //H justify options => 0=left,1=middle,2=right , separate for each column
  //V justify options => 0=top, 1=middle, 2=bottom , for both columns
  //fullFill = true => blacks out whole display, -- VERY EXPENSIVE -- AVOID--
  //sectionFill = true => blacks out entire section-- EXPENSIVE -- USE ONLY WHEN NECESSARY--
  //doUpdate = update the screen (display immediately)
  
  
  void drawSingleRowSection(String leftColText, uint8_t leftColJustify, uint32_t leftColColor, uint32_t leftColBgColor, 
                            String rightColText, uint8_t rightColJustify, uint32_t rightColColor, uint32_t rightColBgColor, 
                            uint8_t vJustify, uint8_t textMaxSize, uint8_t &lastSectionTextSize, 
                            uint8_t sectionYOffset, uint8_t sectionHeight, bool fullFill, bool sectionFill, bool doUpdate
    ) {
      
    uint8_t leftColTextLength = leftColText.length();
    uint8_t rightColTextLength = rightColText.length();
    
    uint8_t colDivider = 2;
    
    uint8_t textSize = 4; //
    uint8_t maxCols=DISPLAY_COLS_TS4; //4 is the max supported size, and DISPLAY_COLS_TS4 are its max columns
    uint8_t xOffset = DISPLAY_XOFFSET;
    uint8_t yOffset = sectionYOffset;
    uint8_t yOffsetAdjust = 0;
    
    uint8_t leftPrefixLength = 0;
    uint8_t rightPrefixLength = 0;
  
  
    if (rightColTextLength == 0) {
      //full width top section
      colDivider = 1;
    }
  
    calculateTextSize(leftColTextLength,rightColTextLength,colDivider,textMaxSize,sectionHeight,textSize,maxCols,xOffset);
    
    if (lastSectionTextSize != textSize) { 
      sectionFill=true; 
    }
    lastSectionTextSize = textSize;
    yOffsetAdjust=(sectionHeight-DISPLAY_FONT_HEIGHT_TS1*textSize)/2;
    yOffset += yOffsetAdjust*vJustify;
    
    
    if (maxCols/colDivider > leftColTextLength){
      if (leftColJustify == 1) {
        //center
        leftPrefixLength = (maxCols/colDivider - leftColTextLength)/2;
      } else if (leftColJustify == 2) {
        //right
        leftPrefixLength = maxCols/colDivider - leftColTextLength;
      }
    }
  
    
    if ((rightColTextLength > 0) && (maxCols/colDivider > rightColTextLength)) {
      if (rightColJustify == 1) {
        //center
        rightPrefixLength = (maxCols/colDivider - rightColTextLength)/2;
      } else if (rightColJustify == 2) {
        //right
        rightPrefixLength = (maxCols/colDivider - rightColTextLength);
      }  
    }
  
    if (fullFill) { 
      clearDisplay();
    }
  
    if (sectionFill) {
        display.fillRect(0,sectionYOffset,DISPLAY_WIDTH/2,sectionHeight,convertRGBAtoRGB565(leftColBgColor));
        display.fillRect(DISPLAY_WIDTH/2,sectionYOffset,DISPLAY_WIDTH,sectionHeight,convertRGBAtoRGB565(rightColBgColor));
    }
  
    #ifdef SSD1331
      display.setTextScale(textSize);   
    #else
      display.setTextSize(textSize);
    #endif
    display.setTextWrap(false);
  
  
    //sumotoy's library has justify built in ( .setCursor(CENTER, CENTER) )
    //you can use that. The following is a generic approach that will work with any library.
    
    //left
    display.setCursor(xOffset, yOffset);
    display.setTextColor(convertRGBAtoRGB565(leftColColor),convertRGBAtoRGB565(leftColBgColor));
    for (int i = 0;i<leftPrefixLength;i++) {
      display.print(" ");
    }
    display.print(leftColText.substring(0,maxCols/colDivider));  
  
    if (rightColTextLength > 0) {
      display.setCursor(DISPLAY_WIDTH/2 + xOffset, yOffset);  
      display.setTextColor(convertRGBAtoRGB565(rightColColor),convertRGBAtoRGB565(rightColBgColor));
      for (int i = 0;i<rightPrefixLength;i++) {
        display.print(" ");
      }
      display.print(rightColText.substring(0,maxCols/colDivider));
    }
  
    #if defined(SSD1306)
      if (doUpdate) { updateDisplay(); }
    #endif
    
  }
  
  void drawSimpleText(String topSectionText, String middleSectionText, String bottomSectionText, uint8_t justify, uint32_t color, uint32_t colorBg) {
    //determine text size, same for all
    uint8_t textSize = 1;  
    uint8_t maxCols = 0;
    uint8_t xOffset = 1;  
    uint8_t textSizeNew = 0;  
    uint8_t yOffset = 0;
    uint8_t lastSectionTextSize = 0;
    
    uint8_t textLength = topSectionText.length();
    calculateTextSize(textLength,0,1,9,displayTopSectionHeight,textSize,maxCols,xOffset);
    textLength = middleSectionText.length();
    calculateTextSize(textLength,0,1,9,displayMainSectionHeight,textSizeNew,maxCols,xOffset);
    if (textSizeNew < textSize) {
      textSize = textSizeNew;
    }
    textLength = bottomSectionText.length();
    calculateTextSize(textLength,0,1,9,displayBottomSectionHeight,textSizeNew,maxCols,xOffset);
    if (textSizeNew < textSize) {
      textSize = textSizeNew;
    }
    //now we know the text size, draw sections after clearing the display
    clearDisplay();
    if (displayTopSectionHeight > 0 && topSectionText.length() > 0 ) {
      drawSingleRowSection(topSectionText,justify,color,colorBg,"",2,color,colorBg,1,textSize,lastSectionTextSize,yOffset,displayTopSectionHeight,false,true,true);       
    }
    
    yOffset=displayTopSectionHeight;
    if (displayMainSectionHeight > 0 && middleSectionText.length() > 0 ) {
      drawSingleRowSection(middleSectionText,justify,color,colorBg,"",2,color,colorBg,1,textSize,lastSectionTextSize,yOffset,displayMainSectionHeight,false,true,true);
    }
    
    yOffset=displayTopSectionHeight+displayMainSectionHeight;
    if (displayBottomSectionHeight > 0 && bottomSectionText.length() > 0 ) {
      drawSingleRowSection(bottomSectionText,justify,color,colorBg,"",2,color,colorBg,1,textSize,lastSectionTextSize,yOffset,displayBottomSectionHeight,false,true,true);
    }
  }
  
  void recalcRocGraphMinMax (double weightGraph[], uint16_t &minPos, uint16_t &maxPos){
    uint16_t newMinPos = 0;
    uint16_t newMaxPos = 0;
  
    for (int i = 0; i < DISPLAY_WIDTH; i++ ){
      if (weightGraph[i] > weightGraph[newMaxPos]) {
        newMaxPos = i;
      }
      if (weightGraph[i] < weightGraph[newMinPos]){
        newMinPos = i;
      }
    }
    minPos = newMinPos;
    maxPos = newMaxPos;
  }
  
  void drawGraph(uint16_t lastGraphPixel[], double weightGraph[],uint8_t sectionHeight, uint8_t yOffset, double weightGraphDivide, uint16_t graphHead, uint32_t color, uint32_t colorBg) {    
        uint16_t displayX = 0;
        int16_t normalizedValue = 0;
  
        
        for (int i=graphHead;i<DISPLAY_WIDTH;i++) {
          normalizedValue = (weightGraph[i]/weightGraphDivide)*sectionHeight;
          if (normalizedValue < 0) {  normalizedValue = 0;  }
          if (displayX <= DISPLAY_WIDTH) {
            //draw only if different from last time
            uint16_t newPixel = yOffset+(sectionHeight-normalizedValue);
            if (newPixel != lastGraphPixel[displayX]){
              display.drawPixel(displayX,lastGraphPixel[displayX],convertRGBAtoRGB565(colorBg));
              if (normalizedValue > 0) {
                display.drawPixel(displayX,newPixel,convertRGBAtoRGB565(color));
                lastGraphPixel[displayX] = newPixel;
              } else {              
                lastGraphPixel[displayX] = 0;
              }
            }
            displayX++; 
          }       
        }
        
        for (int i=0;i<graphHead;i++) {
          normalizedValue = (weightGraph[i]/weightGraphDivide)*sectionHeight;
          if (normalizedValue < 0) {  normalizedValue = 0;  }
          if (displayX <= DISPLAY_WIDTH) {
            //draw only if different from last time
            uint16_t newPixel = yOffset+(sectionHeight-normalizedValue);
            if (newPixel != lastGraphPixel[displayX]){
              display.drawPixel(displayX,lastGraphPixel[displayX],convertRGBAtoRGB565(colorBg));
              if (normalizedValue > 0) {
                display.drawPixel(displayX,newPixel,convertRGBAtoRGB565(color));
                lastGraphPixel[displayX] = newPixel;
              } else {              
                lastGraphPixel[displayX] = 0;
              }
            }
            displayX++; 
          } 
          
        }
        updateDisplay();
  }

  void drawFullDisplay(String grams, String voltage, String resolution, String rateOfChange, bool bleConnected, String lastTare, String timer, bool sectionFill, uint8_t drawWeightGraphInSection, uint8_t hasSettled, uint8_t hrActive) {
    //we need to hold previous values so we can only update sections with changes. Display update is expensive, no need to do it for static sections
    static String _grams = "";
    static String _voltage = "";
    static String _resolution = "";
    static String _resvolt = "";
    static String _rateOfChange = "";
    static bool _bleConnected = false;
    static String _lastTare = "";
    static String _timer = "";
  
    static double weightGraph[DISPLAY_WIDTH];
    static uint16_t lastGraphPixel[DISPLAY_WIDTH];
    static uint16_t graphHead = 0;
    static uint16_t graphTail = 0;
    static uint16_t graphMaxPos = 0;
    static uint16_t graphMinPos = 0;
  
    static double weightGraphDivide = 1.0;
  
    if (resetWeightGraph) {    
      resetWeightGraph = false;
      for (int i=0;i<DISPLAY_WIDTH;i++) {
          weightGraph[i]=0;
      }
    }
  
  
    if (drawWeightGraphInSection > 0) {
      graphTail++;
      if (graphTail >= DISPLAY_WIDTH) {       
        graphTail = 0;
      }
      double newValue;
      newValue = scale.lastUnits;
      if (newValue < 0 ) { newValue = 0; }
      weightGraph[graphTail] = newValue;
      if (graphTail == graphHead) {
        graphHead++;
        if (graphHead >= DISPLAY_WIDTH) {
          graphHead = 0;
        }
      }
      if (graphTail == graphMaxPos || graphTail == graphMinPos) {
        recalcRocGraphMinMax(weightGraph, graphMinPos, graphMaxPos);    
        weightGraphDivide = weightGraph[graphMaxPos];
        if (weightGraphDivide <= 0) {
          weightGraphDivide = 1.0;
        }
      } else {
        if (newValue > weightGraph[graphMaxPos]) {
          graphMaxPos = graphTail;
          if (weightGraphDivide == 0) {
            weightGraphDivide = 1;
          }
        } else if (newValue < weightGraph[graphMinPos]) {
          graphMinPos = graphTail;
        }      
      }  
      
      if (weightGraph[graphMaxPos] > displayTopSectionHeight) {
        weightGraphDivide = weightGraph[graphMaxPos];
      } else {
        weightGraphDivide = 1;
      }
     
    }
  
    
    
    uint8_t yOffset = 0;
    
    if (sectionFill) {
      drawSingleRowSection("",0,colorTop,colorTopBg,"",0,colorTop,colorTopBg,1,DISPLAY_TOP_MAX_TEXT_SIZE,displayTopSectionLastTextSize,yOffset,displayTopSectionHeight,false,true,false);
      yOffset += displayTopSectionHeight;
      drawSingleRowSection("",0,colorMain,colorMainBg,"",0,colorMain,colorMainBg,1,DISPLAY_MAIN_MAX_TEXT_SIZE,displayTopSectionLastTextSize,yOffset,displayMainSectionHeight,false,true,false);
      yOffset += displayMainSectionHeight;
      drawSingleRowSection("",0,colorBottom,colorBottomBg,"",0,colorBottom,colorBottomBg,1,DISPLAY_BOTTOM_MAX_TEXT_SIZE,displayTopSectionLastTextSize,yOffset,displayBottomSectionHeight,false,true,true);    
      sectionFill = false; 
    }
  
  
    
    if (displayTopSectionHeight > 0) {
      sectionFill = false;
      //update top section
      yOffset=0;   
      if (drawWeightGraphInSection == 1) {
        drawGraph(lastGraphPixel,weightGraph,displayTopSectionHeight,yOffset,weightGraphDivide,graphHead,colorTop,colorTopBg);
      } else {    
        if (lastTare != _lastTare || timer != _timer || bleConnected != _bleConnected) {
            if (lastTare.length() == 0 || timer.length() == 0 || _lastTare.length() != lastTare.length() || _timer.length() != timer.length() || bleConnected != _bleConnected){
              sectionFill = true;
            }
            drawSingleRowSection(lastTare,0,colorTop,colorTopBg,timer,1,colorTop,colorTopBg,1,DISPLAY_TOP_MAX_TEXT_SIZE,displayTopSectionLastTextSize,yOffset,displayTopSectionHeight,false,sectionFill,true);
            if (bleConnected) {
              display.drawBitmap(DISPLAY_WIDTH-16, yOffset+2, bleLogo, 16, 12, convertRGBAtoRGB565(colorTop));
              updateDisplay();
            }
          _lastTare = lastTare;
          _timer = timer;
          _bleConnected = bleConnected;
        }    
      }
    } //maybe we want to draw this info somewhere else if top not available ???
  
    if (displayMainSectionHeight > 0) {
      sectionFill = false;
      yOffset=displayTopSectionHeight;
      if (drawWeightGraphInSection == 2) {
        drawGraph(lastGraphPixel,weightGraph,displayMainSectionHeight,yOffset,weightGraphDivide,graphHead,colorMain,colorMainBg);
      } else {   
        if (grams != _grams) {
          //update main section
          if (grams.length() == 0 || _grams.length() != grams.length()){
            sectionFill = true;
          }
            drawSingleRowSection(grams,2,colorMain,colorMainBg,"",2,colorMain,colorMainBg,1,DISPLAY_MAIN_MAX_TEXT_SIZE,displayMainSectionLastTextSize,yOffset,displayMainSectionHeight,false,sectionFill,true);
          _grams = grams;    
        }
        if (1==1) {
          if (hasSettled) {
            display.drawPixel(DISPLAY_WIDTH-4,yOffset,convertRGBAtoRGB565(colorMain));
            display.drawPixel(DISPLAY_WIDTH-3,yOffset,convertRGBAtoRGB565(colorMain));
            display.drawPixel(DISPLAY_WIDTH-2,yOffset,convertRGBAtoRGB565(colorMain));
          } else {
            display.drawPixel(DISPLAY_WIDTH-4,yOffset,convertRGBAtoRGB565(colorMainBg));
            display.drawPixel(DISPLAY_WIDTH-3,yOffset,convertRGBAtoRGB565(colorMainBg));
            display.drawPixel(DISPLAY_WIDTH-2,yOffset,convertRGBAtoRGB565(colorMainBg));
          }
          if (hrActive) {
            display.drawPixel(DISPLAY_WIDTH-4,yOffset + 2,convertRGBAtoRGB565(colorMain));
            display.drawPixel(DISPLAY_WIDTH-2,yOffset + 2,convertRGBAtoRGB565(colorMain));
          } else {
            display.drawPixel(DISPLAY_WIDTH-4,yOffset + 2,convertRGBAtoRGB565(colorMainBg));
            display.drawPixel(DISPLAY_WIDTH-2,yOffset + 2,convertRGBAtoRGB565(colorMainBg));
          }
          updateDisplay();       
        }
      }
    }
    
    if (displayBottomSectionHeight > 0) {
      sectionFill = false;
      //update bottom section
      yOffset=displayTopSectionHeight+displayMainSectionHeight;
      if (drawWeightGraphInSection == 3) {
        drawGraph(lastGraphPixel,weightGraph,displayBottomSectionHeight,yOffset,weightGraphDivide,graphHead,colorBottom,colorBottomBg);
      } else {   
        if (rateOfChange != _rateOfChange || resolution != _resolution || voltage != _voltage) {
          String resvolt = resolution + voltage;
            if (rateOfChange.length() == 0 || resvolt.length() == 0 || _rateOfChange.length() != rateOfChange.length() || _resvolt.length() != resvolt.length()){
              sectionFill = true;
            }
            drawSingleRowSection(rateOfChange,0,colorBottom,colorBottomBg,resvolt,2,colorBottom,colorBottomBg,1,DISPLAY_BOTTOM_MAX_TEXT_SIZE,displayBottomSectionLastTextSize,yOffset,displayBottomSectionHeight,false,sectionFill,true);
          _rateOfChange = rateOfChange;
          _resolution = resolution;
          _voltage = voltage;
          _resvolt = resvolt;
        }
      }
  
    }
  
  }
#endif
