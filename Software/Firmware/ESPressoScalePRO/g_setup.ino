/* 
   \\\\\\\\\\\\\\\\\\\\    7.setup()    ////////////////////
*/


void setup() {  

  #ifdef SERIAL_IF
    //pinMode(3,INPUT_PULLUP);
    Serial.begin(115200);
    LOG_PRINTLN("ESPresso Scale w/ Serial Enabled");    
  #endif

  //before antything else, detect power + tare button pressing
  //if yes, for 10 seconds, reset settings
  
  pinMode(TARE_BUTTON_PIN,INPUT_RESISTOR);

  uint32_t duration_elapsed = 0;
  uint32_t boot_init_millis = millis();
  
  while (digitalRead(TARE_BUTTON_PIN) == BUTTON_TRUE) {
    toggleStatusLed(true);
    duration_elapsed = millis() - boot_init_millis;
    if (5-duration_elapsed/1000 <= 0) {
      DEBUG_PRINTLN("release buttons to reset settings");
      forceResetEEPROMSettings = 1;      
      toggleStatusLed(false);
    } else {
      DEBUG_PRINT("keep pressing punk...will reset in ");DEBUG_PRINTLN(5-duration_elapsed/1000);      
    }
    delay(10);
  }

  upgradeMode = 0;
  wifiIP = "";
  if (!settings.loadSettings() || forceResetEEPROMSettings) {
    DEBUG_PRINTLN("resetting all settings");
    settings.resetSettings();
    //no need to apply settings, we are in setup()
  }

  //TESTING // ALWAYS RESET  
  //resetSettings();

  colorMain = settings.colorMain;
  colorTop = settings.colorTop;
  colorBottom = settings.colorBottom;
  colorMainBg = settings.colorMainBg;
  colorTopBg = settings.colorTopBg;
  colorBottomBg = settings.colorBottomBg;
  
  displayRotation = settings.displayRotation;
  displayMaxBr = settings.displayMaxBr;

  clickThreshold = settings.clickThreshold;
  lClickThr = settings.lClickThr;


  displayTopSectionHeight = settings.displayTopH;
  displayMainSectionHeight = settings.displayMainH;
  displayBottomSectionHeight = settings.displayBottomH;
  
  if (displayTopSectionHeight + displayMainSectionHeight + displayBottomSectionHeight == 0) {
    //why ??? probably error
    displayMainSectionHeight = 16; //failsafe
  }
  
  
    #ifdef MINI_LIFEPO
      pinMode(ADC_LDO_EN_PIN, OUTPUT);
      digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_ENABLE);
      delay(500);
    #endif
    
  #ifdef SSD1306
    #ifdef SSD1306_WIRE
      Wire.begin(DISPLAY_MOSI_PIN, DISPLAY_CLK_PIN);
      DEBUG_PRINTLN("Wire Init");
      if(!display.begin(SSD1306_SWITCHCAPVCC,0x3C)) {
        LOG_PRINTLN("SSD1306 allocation failed");
      }
    #else
      spiDisplay.begin(14,12,13,15);
      if(!display.begin(SSD1306_SWITCHCAPVCC)) {
        LOG_PRINTLN("SSD1306 allocation failed");
      }    
    #endif   
    delay(200);
    clearDisplay();
    display.setRotation(displayRotation);  
    if (settings.bootTextTop == "" && settings.bootTextMiddle == "" && settings.bootTextBottom == "") {
      display.drawBitmap((DISPLAY_WIDTH-64)/2, (DISPLAY_HEIGHT-32)/2, bootLogo, 64, 32, convertRGBAtoRGB565(colorMain));
    } else {
      drawSimpleText(settings.bootTextTop,settings.bootTextMiddle,settings.bootTextBottom,1,colorMain,colorMainBg);
    }
    display.display();
  #endif

  #ifdef SSD1331
    display.begin();
    display.setBackground(convertRGBAtoRGB565(COLOR_BLACK));
    display.setBrightness(255);
    display.setFont(&Terminal_9);
    display.setRotation(displayRotation);  
    clearDisplay();
    display.drawBitmap((DISPLAY_WIDTH-64)/2, (DISPLAY_HEIGHT-32)/2, bootLogo, 64, 32, convertRGBAtoRGB565(colorMain));
  #endif

  #ifdef ST7735
    spiDisplay.begin(14,12,13,15);
    //pinMode(DISPLAY_BACKLIGHT, OUTPUT);
    //digitalWrite(DISPLAY_BACKLIGHT, HIGH); // Backlight on 100%
    ledcAttachPin(DISPLAY_BACKLIGHT_PIN, 1); //Dynamic backlight control , simply call ledcWrite(1,byte);
    ledcSetup(1, 100, 8);
    ledcWrite(1, displayMaxBr);    
    display.initR(DISPLAY_INITR);
    display.invertDisplay(DISPLAY_INVERT);
    display.setRotation(displayRotation);
    clearDisplay();  
    display.drawBitmap((DISPLAY_WIDTH-64)/2, (DISPLAY_HEIGHT-32)/2, bootLogo, 64, 32, convertRGBAtoRGB565(colorMain));
  #endif
  #ifndef LEDSEGMENT
    setRotation(displayRotation);
  #endif

  #ifdef LEDSEGMENT
    DEBUG_PRINTLN("Initialising LED Display");
    #ifdef LUNAR_ECLIPSE_5
      pinMode(LED_LDO_EN_PIN, OUTPUT);
      digitalWrite(LED_LDO_EN_PIN,LED_LDO_ENABLE);  
      delay(500);
    #endif
    lc.shutdown(LED_ADDR,false);
    if (displayMaxBr > 15) {
      displayMaxBr = 15;
    }
    lc.setIntensity(LED_ADDR,displayMaxBr);   
    clearDisplay();  
    lc.setChar(LED_ADDR,0,'0',false);
    lc.setChar(LED_ADDR,1,'5',false);
    lc.setChar(LED_ADDR,2,'E',false);
    lc.setChar(LED_ADDR,3,'5',false);
  #endif

  #ifdef BLE
    if (settings.bleEnabled) {
      // Create the BLE Device
      BLEDevice::init(BLE_DEVICE_NAME);
    
      // Create the BLE Server
      pServer = BLEDevice::createServer();
      pServer->setCallbacks(new bleServerCallbacks());
    
      // Create the BLE Service
      BLEService *pService = pServer->createService(BLE_SERVICE_UUID);
    
      // Create a BLE Characteristic
      pWeightTimerCharacteristic = pService->createCharacteristic(
                          BLE_CHARACTERISTIC_WEIGHT_TIMER_UUID,
                          BLECharacteristic::PROPERTY_READ   |
                          BLECharacteristic::PROPERTY_NOTIFY
                        );
      pSettingsCharacteristic = pService->createCharacteristic(
                          BLE_CHARACTERISTIC_SETTINGS_UUID,
                          BLECharacteristic::PROPERTY_READ |
                          BLECharacteristic::PROPERTY_WRITE
                        );
                          
      pWeightTimerCharacteristic->setCallbacks(new bleWeightTimerCallbacks());
      pSettingsCharacteristic->setCallbacks(new bleSettingsCallbacks());
      
      // https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.descriptor.gatt.client_characteristic_configuration.xml
      // Create a BLE Descriptor
      pWeightTimerCharacteristic->addDescriptor(new BLE2902());
      pSettingsCharacteristic->addDescriptor(new BLE2902());
    
      // Start the service
      pService->start();
      
      // Start advertising
      BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
      pAdvertising->addServiceUUID(BLE_SERVICE_UUID);
      pAdvertising->setScanResponse(false);
      pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
      BLEDevice::startAdvertising();
      DEBUG_PRINTLN("BLE // Advertising");
    }
  #endif

  //initiating OTA
  if (settings.otaUpgrade == 1) {        // Connect to WiFi network
    upgradeMode=1;
    //we got otaUpdate from EEPROM so for this run, we will do what it says.
    //however, we immediately want to reset it on EEPROM so on our next reboot will be 0 again
    settings.saveSettingByte("otaUpgrade",0);
    
    #ifdef OTA
      WiFi.begin(settings.wifiSSID.c_str(), settings.wifiPassword.c_str());
      DEBUG_PRINT("connecting to ");DEBUG_PRINT(settings.wifiSSID);DEBUG_PRINT(" with pass ");DEBUG_PRINTLN(settings.wifiPassword);

      #ifndef LEDSEGMENT
        drawSimpleText("Connecting to",String(settings.wifiSSID),"",1,colorMain,colorMainBg);
      #endif
      #ifdef LEDSEGMENT
        lc.clearDisplay(LED_ADDR);
        lc.setChar(LED_ADDR,0,'U',false);
        lc.setChar(LED_ADDR,1,'P',false);
        lc.setChar(LED_ADDR,2,'d',false);
      #endif
  
      if (settings.bleEnabled == 1) {
        pWeightTimerCharacteristic->setValue("UPGRADE");
        pWeightTimerCharacteristic->notify();
      }
  
      uint32_t wifiConnectionStart = millis();
    
      // Wait for connection
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        yield();
        DEBUG_PRINT(".");
        if (millis() - wifiConnectionStart > WIFI_CONNECTION_TIMEOUT*1000) {
          ESP.restart();
        }
      }
      LOG_PRINTLN("");
      LOG_PRINT("Connected to ");
      LOG_PRINTLN(settings.wifiSSID);
      LOG_PRINT("IP address: ");
      LOG_PRINTLN(WiFi.localIP().toString());
      wifiIP = WiFi.localIP().toString();    
      #ifndef LEDSEGMENT
        drawSimpleText("IP:",String(wifiIP),"",1,colorMain,colorMainBg);
      #endif
  
      server.on("/", HTTP_GET, []() {
        server.sendHeader("Connection", "close");
        server.send(200, "text/html", HTTP_SRV_LOGIN_HTML);
      });
      server.on("/main", HTTP_GET, []() {
        server.sendHeader("Connection", "close");
        server.send(200, "text/html", HTTP_SRV_MAIN_HTML);
      });
      /*handling uploading firmware file */
      server.on("/update", HTTP_POST, []() {
        Serial.printf("Update error: closing connection");
        server.sendHeader("Connection", "close");
        server.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
        ESP.restart();
      }, []() {
        HTTPUpload& upload = server.upload();
        if (upload.status == UPLOAD_FILE_START) {
          Serial.printf("Update: %s\n", upload.filename.c_str()); 
          #ifdef LEDSEGMENT
            lc.clearDisplay(LED_ADDR); 
            //G
            lc.setRow(LED_ADDR,1,B01011111);
            lc.setChar(LED_ADDR,2,'0',false);
            lc.setChar(LED_ADDR,3,'0',false);
            lc.setChar(LED_ADDR,4,'d',false);
          #endif
          #ifndef LEDSEGMENT
            drawSimpleText("UPGRADING","---------",String(wifiIP),1,colorMain,colorMainBg);
          #endif
          if (!Update.begin(UPDATE_SIZE_UNKNOWN)) { //start with max available size
            Serial.printf("Update error: UPDATE_SIZE_UNKNOWN");
            Update.printError(Serial);
          }
        } else if (upload.status == UPLOAD_FILE_WRITE) {
          /* flashing firmware to ESP*/
          if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
            Serial.printf("Update error: %u\nRebooting...\n", upload.currentSize);
            Update.printError(Serial);
          }
        } else if (upload.status == UPLOAD_FILE_END) {
          if (Update.end(true)) { //true to set the size to the current progress
            Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
          } else {
            Update.printError(Serial);
          }
        }
      });
      server.begin();  
     #endif
  } else {
    //only proceed if not on OTA upgrade mode
    //Enable the Analog voltage LDO
    #ifdef PRO
      pinMode(ADC_LDO_EN_PIN, OUTPUT);
      digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_ENABLE);
    #endif
    #ifdef LUNAR_ECLIPSE_5
      pinMode(ADC_LDO_EN_PIN, OUTPUT);
      digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_ENABLE);
    #endif
  
    scale.begin(0,128,settings.adcSpeed);
    
    //Apply scale options we got from EEPROM
    scale.zeroRange = (float)settings.zeroRange/100.0;
    scale.zeroTracking = (float)settings.zeroTracking/100.0;
    scale.fakeStabilityRange = (float)settings.fakeRange/100.0;
    scale.decimalDigits = settings.decimalDigits;
    scale.setSensitivity(settings.sensitivity);
    scale.setSmoothing(settings.smoothing);
    scale.setCalFactor(settings.calFactorULong/10.0);
    scale.autoTare = settings.autoTare;
    scale.aTareNeg = settings.aTareNeg;
    scale.autoStartTimer = settings.autoStartTimer;
    scale.autoStopTimer = settings.autoStopTimer;
    scale.timerDelay = settings.timerDelay*1000;
    scale.tmrStartW = (float)settings.tmrStartW/10.0;
    scale.timerStopWeight = (float)settings.timerStopWeight;
    scale.rocStartTimer = (float)settings.rocStartTimer/10.0;
    scale.rocStopTimer = (float)settings.rocStopTimer/10.0;
    scale.setHRMode(settings.hrMode);
    scale.setHRAutoSwitchSpeed(settings.hrAutoSwSpd);
    scale.setHRPeriod(settings.hrPeriod);
    scale.tare(2,false,false,true);

  }

  //Status LED
  #ifdef STATUS_LED
    pinMode(STATUS_LED_PIN,OUTPUT);    
  #endif

  attachAllButtonInterrupts();
    

  initialising = true;  
  #ifdef TOUCH
    esp_sleep_enable_touchpad_wakeup();
  #endif
  #ifdef BUTTON
    esp_sleep_enable_ext0_wakeup(POWER_BUTTON_PIN_RTC, 1); //1 = High, 0 = Low
  #endif

  #ifdef MONITOR_VOLTAGE
    vinVoltage = monitoring.getVoltage();
    resolutionLevel = monitoring.getResolutionLevel(vinVoltage);
    lastVinRead = millis();
  #endif

  displayRefreshInterval = 1000/settings.displayFps;
  bleRefreshInterval = 1000/settings.bleNps;
  batReadInterval = settings.batReadInterval;
  graphInSection = settings.graphInSection;
  tareButtonDelay = settings.tareButtonDelay;

  
  lastActionMillis = millis();
  

  
}
