/*
   \\\\\\\\\\\\\\\\\\\\    8.loop()    ////////////////////
*/
void loop() {


#ifdef SERIAL_IF
  if (Serial.available() > 0) {
    int INPUT_SIZE = 30;
    char input[INPUT_SIZE + 1];
    uint8_t size = Serial.readBytes(input, INPUT_SIZE);
    input[size] = 0;
    char* separator = strchr(input, '=');
    if (separator != 0)
    {
      *separator = 0;
      String command = input;
      ++separator;
      if (strlen(separator) > 1) {
        separator[strlen(separator) - 1] = '\0';
        DEBUG_PRINT("command "); DEBUG_PRINTLN(command);
        DEBUG_PRINTLN(handleCommand(command, separator));
      } else {
        //assume empty string
        DEBUG_PRINTLN(handleCommand(command, ""));
      }
    }
  }
#endif


  if (settings.otaUpgrade == 1) {
    if (initialising) {
      initialising = false;
#ifdef LEDSEGMENT
      lc.clearDisplay(LED_ADDR);
      //uppercase u letter in digit 1
      lc.setRow(LED_ADDR, 1, B00111110);
      lc.setChar(LED_ADDR, 2, 'P', false);
      lc.setChar(LED_ADDR, 3, 'd', false);
#endif
#ifndef LEDSEGMENT
      drawSimpleText("UPGRADE", String(wifiIP),"", 1, colorMain, colorMainBg);
#endif
    }
#ifdef OTA
    server.handleClient();
#endif
    handlePowerButton();
    //    delay(100);
    return;
  }






  if (millis() > (lastButtonDebounceUpdate + BUTTON_DEBOUNCE_INTERVAL)) {
    lastButtonDebounceUpdate += BUTTON_DEBOUNCE_INTERVAL;
    if (POWER_BUTTON_PIN > 0) {
      handlePowerButton();
    }
    if (TARE_BUTTON_PIN > 0) {
      handleTareButton();
    }
    if (SECONDARY1_BUTTON_PIN > 0) {
      handleSecondary1Button();
    }
  }



  if (lightSleep && wakeup) {
    wakeUpFromLightSleep();
  }

  if (snooze && wakeup) {
    snooze = false;
    displayExtraRefreshInterval = 0;
    wakeup = false;
#ifdef ST7735
    ledcWrite(1, displayMaxBr);
#endif
#ifdef LEDSEGMENT
    if (displayMaxBr > 15) {
      displayMaxBr = 15;
    }
    lc.setIntensity(LED_ADDR, displayMaxBr);
#endif
    lastActionMillis = millis();
  }


  // !lightSleep if
  // Don't do any of the following if already sleeping in order to conserve power
  // If you need something to update during light sleep, remove it from the following if
  // The only way to wake up from light sleep is if you set wakeup = true , ex. from a button or from touch interrupt (or any other interrupt)
  // PRO pcb consumes about 40mA during light sleep
  if (!lightSleep) {
    //While initialising, keep taring... (you can disable that, ADS1232 is fine with only the basic initialising done on the library)
    if (millis() > INITIALISING_SECS * 1000 && initialising) {
      initialising = false;
      scale.tare(2, false, true, true);
      clearDisplay();
    }
    if (initialising) {
      //tare with false,false will also do a calibration on the ADC
      //      scale.tare(2,false,true,true);
      lastActionMillis = millis();
#ifdef LEDSEGMENT
      lc.clearDisplay(LED_ADDR);
      lc.setChar(LED_ADDR, 0, '0', false);
      lc.setChar(LED_ADDR, 1, '5', false);
      lc.setChar(LED_ADDR, 2, 'E', false);
      lc.setChar(LED_ADDR, 3, '5', false);
#endif
      DEBUG_PRINTLN("initializing...");
      scale.readUnits(settings.readSamples);
      return;
    }


    if (beginTare > 0 && millis() > beginTare) {
      DEBUG_PRINTLN("taring from main loop...");
      beginTare = 0;
      scale.tare(tareType, saveWeight, false, calibrateTare); //this command is from a manual/user trigger
      tareType = 0;
      saveWeight = false;
      calibrateTare = false;
      resetWeightGraph = true;
    }

    if (calibrateToUnits > 0.0) {
      calibrationMode = true;
      lastActionMillis = millis();
      DEBUG_PRINT("should calibrate to "); DEBUG_PRINTLN(calibrateToUnits);
#ifdef LEDSEGMENT
      writeTo7Seg(calibrateToUnits, 6, 0);
      lc.setChar(LED_ADDR, 0, 'C', false);
#endif
#ifndef LEDSEGMENT
      drawSimpleText("Calibration", "will begin", "automatically", 0, colorMain, colorMainBg);
      delay(1000);
      drawSimpleText("Place", String(calibrateToUnits) + "g", "and wait", 1, colorMain, colorMainBg);
#endif

      scale.calibrate(calibrateToUnits, 30000, 0.05);
      DEBUG_PRINT("our old calfactor "); DEBUG_PRINT(settings.calFactorULong / 10.0);
      settings.calFactorULong = (uint32_t)(scale.getCalFactor() * 10.0);
      DEBUG_PRINT(" our new calfactor "); DEBUG_PRINTLN(settings.calFactorULong / 10.0);
      settings.saveSettingULong("calFactorULong", settings.calFactorULong);
      calibrateToUnits = 0.0;
#ifndef LEDSEGMENT
      drawSimpleText("", "THANKS !", "", 1, colorMain, colorMainBg);
#endif
      calibrationMode = false;
    }

    String hrBLEStr = "";

    if (!snooze || (millis() > (lastAdcRead + ADC_SNOOZE_DELAY))) {
      gramsDbl = scale.readUnits(settings.readSamples);
      lastAdcRead = millis();
      //scale library is handling the rounding but if we really want to show the proper amount of decimals
      //we must fix our string properly. If you do String(grams) you cannot get >2 decimals.
      char buffer[20];
      for (uint8_t i = 0; i < 20; i++) {
        buffer[i] = 'c';
      }
      dtostrf (gramsDbl, 6, scale.decimalDigits, buffer);
      gramsStr = String(buffer);

      for (uint8_t i = 0; i < 20; i++) {
        buffer[i] = 'c';
      }
      double rocDbl = scale.roc;
      dtostrf (rocDbl, 6, scale.decimalDigits, buffer);
      rocStr = String(buffer);

      for (uint8_t i = 0; i < 20; i++) {
        buffer[i] = 'c';
      }
      dtostrf (scale.firstHRUnits, 6, scale.decimalDigits, buffer);
      firstHRUnitsStr = String(buffer);


      if (scale.hasSettled) {
        if (scale.hrActive) {
          hrBLEStr = "100";

        } else if (scale.hrEnabled) {
          hrBLEStr = scale.hrStatuspc;
        }
      }

    }

    if (redrawDisplay) {
      redrawDisplay = false;
      clearDisplay();
#ifndef LEDSEGMENT
      drawFullDisplay("", "", "", "", false, "", "", true, 0, 0, 0);
#endif
    }

    if (millis() > (lastDisplayUpdate + displayRefreshInterval + displayExtraRefreshInterval)) {
      lastDisplayUpdate += displayRefreshInterval + displayExtraRefreshInterval;
      DEBUG_PRINT(gramsStr); DEBUG_PRINT("g");

      if (scale.hasSettled) {
        if (scale.hrActive) {
          hrBLEStr = "100";
          DEBUG_PRINT(" (s+HR) ");

        } else {
          DEBUG_PRINT(" (s) ");
          if (scale.hrEnabled) {
            DEBUG_PRINT(scale.hrStatuspc);
            hrBLEStr = scale.hrStatuspc;
            DEBUG_PRINT("% HR");
          }
        }
        DEBUG_PRINT(" (");
      } else {
        DEBUG_PRINT(" (");
      }
      DEBUG_PRINT(scale.lastTareWeightRounded); DEBUG_PRINT("g)   ///   ");


      //DEBUG_PRINT(scale.roc);DEBUG_PRINT(" g/s // ");
      DEBUG_PRINT(rocStr); DEBUG_PRINT(" g/s // ");
      DEBUG_PRINT(scale.getAdcActualSPS()); DEBUG_PRINT("SPS   ///   ");

      timerInSeconds = scale.getTimer() / 1000.f;
      timerInSeconds = roundToDecimal(timerInSeconds, 1);
      if (timerInSeconds == 0) {
        timerStr = " ";
      } else if ( timerInSeconds > 0 || settings.tmrShowNeg) {
        char buffer[20];
        for (uint8_t i = 0; i < 20; i++) {
          buffer[i] = 'c';
        }
        dtostrf (timerInSeconds, 1, 1, buffer);
        timerStr = String(buffer);
        timerStr = timerStr + "s";
        DEBUG_PRINT(timerInSeconds); DEBUG_PRINT("  /-/   ");
      } else if (timerInSeconds < 0) {
        if (scale.timerRunning) {
          DEBUG_PRINT(timerStr);
          //          DEBUG_PRINT("  ///   ");
        }
      }
      //      DEBUG_PRINT(vinVoltage);DEBUG_PRINT("V (");DEBUG_PRINT(resolutionLevel);DEBUG_PRINTLN(")");
      DEBUG_PRINTLN("");




      if (scale.timerRunning && timerInSeconds < 0) {
        //pulse dot only if our timer is actually running
        if (timerStr == " ") {
          timerStr = ".";
        } else if (timerStr == ".") {
          timerStr = " ";
        }
      } else {
        if (timerStr == ".") {
          timerStr = " ";
        }
      }
      if (!initialising) {
#ifndef LEDSEGMENT
        drawFullDisplay(gramsStr + "g", "" /*voltage*/, "" /*resolution*/, String(scale.roc) + "g/s", deviceConnected, String(scale.lastTareWeightRounded) + "g", timerStr, false, graphInSection, scale.hasSettled, scale.hrActive);
#endif
#ifdef LEDSEGMENT
        writeTo7Seg(gramsDbl, LED_COUNT, scale.decimalDigits);
#endif
      }
    }


#ifdef BLE
    if (settings.bleEnabled) {
      //BLE notify loop
      if (deviceConnected && (millis() > lastBleUpdate + bleRefreshInterval)) {
        lastBleUpdate += bleRefreshInterval;
        String BLENotifyStr = gramsStr + "|" + timerStr + "|" + hrBLEStr;
        //DEBUG_PRINT("BLE NOT -> "); DEBUG_PRINTLN(BLENotifyStr);
        pWeightTimerCharacteristic->setValue(BLENotifyStr.c_str());
        pWeightTimerCharacteristic->notify();
      }
      // BLE disconnecting
      if (!deviceConnected && oldDeviceConnected) {
        pServer->startAdvertising(); // restart advertising
        DEBUG_PRINTLN("start advertising");
        oldDeviceConnected = false;
      }
      // BLE connecting
      if (deviceConnected && !oldDeviceConnected) {
        // do stuff here on connecting
        oldDeviceConnected = deviceConnected;
      }
    }
#endif

    if (scale.hasSettled) {
      //enable the following check if you want to only snooze when there is no weight on the scale
      //      if (fabs(grams) < 10){
      ////        DEBUG_PRINTLN("don't leave me alone please");
      //      } else {
      //        wakeup = true;
      //        lastActionMillis = millis();
      //      }
    } else {
      wakeup = true;
      lastActionMillis = millis();
    }



    if (settings.lSlpTimeout > 0 && (millis() - lastActionMillis) >  settings.lSlpTimeout * 10000) {
      initiateLightSleep();
    }


    if (settings.snoozeTimeout > 0 && !snooze && ((millis() - lastActionMillis) >  settings.snoozeTimeout * 10000)) {
      dimDisplay();
      DEBUG_PRINTLN("ZZZZzzzzz");
      snooze = true;
      wakeup = false;
      displayExtraRefreshInterval = ADC_SNOOZE_DELAY - displayRefreshInterval;
    }

#ifdef MONITOR_VOLTAGE
    if (millis() > (lastVinRead + batReadInterval * 1000)) {
      vinVoltage = monitoring.getVoltage();
      resolutionLevel = monitoring.getResolutionLevel(vinVoltage);
      lastVinRead += batReadInterval * 1000;
    }
#endif


    //END OF !lightSleep if
  }

  if (deepSleepNow || (settings.dSlpTimeout > 0 && (millis() - lastActionMillis) >  settings.dSlpTimeout * 10000)) {
    initiateDeepSleep();
  }


}
