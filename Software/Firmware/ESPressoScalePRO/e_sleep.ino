/* 
   \\\\\\\\\\\\\\\\\\\\    5.SLEEP    ////////////////////
*/
const uint16_t ADC_SNOOZE_DELAY = 250; //250ms delay for each loop function (NOT used with delay() )

uint32_t lastAdcRead = 0;

////////// 5.1.DEEP SLEEP //////////
void initiateDeepSleep() {
  //We must inform the user, wait to debounce the button and then sleep.
  //if we sleep while power is pressed, we will wake up again.
  //shutdown display immediately
  shutDownDisplay();
  #ifdef STATUS_LED
    //pulse status led until the user releases the power button    
    bool value = false;
    while(digitalRead(POWER_BUTTON_PIN)) {
      value = !value;
      toggleStatusLed(value);
      delay(500);
    }
  #endif
  #ifdef PRO
    //disable our LDO
    digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_DISABLE);
  #endif

  
  #ifdef MINI_LIFEPO    
    digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_DISABLE);    
  #endif
  
  #ifdef LUNAR_ECLIPSE_5
    digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_DISABLE);
  #endif
  disableSecondaryButtons();
  #ifdef SERIAL_IF
    Serial.flush();
  #endif
  delay(250);
  
  pinMode(ADC_SCLK_PIN, INPUT);
  pinMode(ADC_SPEED_PIN, INPUT);

  #ifndef LEDSEGMENT
    pinMode(DISPLAY_MOSI_PIN, INPUT);
    pinMode(DISPLAY_CLK_PIN, INPUT);
    pinMode(DISPLAY_DC_PIN, INPUT);
    pinMode(DISPLAY_CS_PIN, INPUT);
    pinMode(DISPLAY_RESET_PIN, INPUT);   
  #endif
  esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_SLOW_MEM, ESP_PD_OPTION_OFF);
  esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_FAST_MEM, ESP_PD_OPTION_OFF);
  esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);
  const int ext_wakeup_pin_1 = 12;
  const uint64_t ext_wakeup_pin_1_mask = 1ULL << ext_wakeup_pin_1;
  esp_sleep_enable_ext1_wakeup(ext_wakeup_pin_1_mask, ESP_EXT1_WAKEUP_ANY_HIGH);
  //rtc_gpio_pulldown_en(GPIO_NUM_12);

  
  esp_deep_sleep_start();
}


////////// 5.2.LIGHT SLEEP //////////
void initiateLightSleep() {
  DEBUG_PRINTLN("shutting down ADC and display");
  #ifdef PRO
    //disable our LDO
    digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_DISABLE);
  #endif
  #ifdef MINI_LIFEPO
    digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_DISABLE);
  #endif
  #ifdef LUNAR_ECLIPSE_5
    digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_DISABLE);
  #endif
  disableSecondaryButtons();
  lightSleep = true;
  wakeup = false;
  scale.powerOff();
  shutDownDisplay();
  #ifdef STATUS_LED  
    //keep status led on to inform the user that we are NOT on deep sleep
    toggleStatusLed(true);
  #endif
  //https://github.com/espressif/esp-idf/issues/2070
  //btStop();
}

void wakeUpFromLightSleep() {
  #ifdef STATUS_LED
    toggleStatusLed(false);
    delay(150);
    toggleStatusLed(true);
    delay(150);
    toggleStatusLed(false);
  #endif
  #ifdef PRO
    //enable our LDO
    digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_ENABLE);
  #endif
  #ifdef MINI_LIFEPO
    digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_ENABLE);
  #endif
  #ifdef LUNAR_ECLIPSE_5
    digitalWrite(ADC_LDO_EN_PIN,ADC_LDO_ENABLE);
  #endif
  delay(50);
  enableSecondaryButtons();
  lightSleep = false;
  snooze = false;
  displayExtraRefreshInterval = 0;
  wakeup = false;
  scale.powerOn();
  wakeUpDisplay();
  lastActionMillis = millis();  
  //https://github.com/espressif/esp-idf/issues/2070
  //btStart();
}
