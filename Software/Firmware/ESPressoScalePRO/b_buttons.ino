/* 
   \\\\\\\\\\\\\\\\\\\\    2.BUTTONS    ////////////////////
*/

////////// 2.1.OPTIONS //////////
uint32_t clickThreshold = 10; //anything below 10ms is ignored
uint32_t lClickThr = 3000; // in ms, below that is simple click, above or equal is long click
const uint16_t BUTTON_DEBOUNCE_INTERVAL = 200; //in ms

uint32_t tareButtonDelay = 0; //default delay when taring with a button


uint32_t lastButtonDebounceUpdate = 0;
//true if user still touching, false if not => true is set in interrupt callback
bool powerButtonPressing = false;
bool tareButtonPressing = false;
bool secondary1ButtonPressing = false;

//millis from 1st touch detected => set in interrupt callback
uint32_t powerButtonPressedStartTime = 0;
uint32_t tareButtonPressedStartTime = 0;
uint32_t secondary1ButtonPressedStartTime = 0;




////////// 2.2.TOUCH CALLBACKS //////////
void touchPowerCallback(){
  powerButtonPressing = true;  
  if (powerButtonPressedStartTime <= 0) {
    powerButtonPressedStartTime = millis();
  }
}

void touchTareCallback(){
  tareButtonPressing = true;
  if (tareButtonPressedStartTime <= 0) {
    tareButtonPressedStartTime = millis();
  }
}

void touchSecondary1Callback(){
  secondary1ButtonPressing = true;
  if (secondary1ButtonPressedStartTime <= 0) {
    secondary1ButtonPressedStartTime = millis();
  }
}



////////// 2.3.ISR CALLBACKS //////////
void IRAM_ATTR powerButtonISR() {
  powerButtonPressing = true;
  if (powerButtonPressedStartTime <= 0) {
    powerButtonPressedStartTime = millis();
  }
}

void IRAM_ATTR tareButtonISR() {
  tareButtonPressing = true;  
  if (tareButtonPressedStartTime <= 0) {
    tareButtonPressedStartTime = millis();
  }
}
void IRAM_ATTR secondary1ButtonISR() {
  secondary1ButtonPressing = true;  
  if (secondary1ButtonPressedStartTime <= 0) {
    secondary1ButtonPressedStartTime = millis();
  }
}




////////// 2.4.ENABLE/DISABLE BUTTONS //////////
void disableSecondaryButtons() {
  //disable all interrupts except power
  if (TARE_BUTTON_PIN > 0) {
    #ifdef TOUCH
      touchAttachInterrupt(TARE_BUTTON_PIN, touchTareCallback, 0);
    #endif
    #ifdef BUTTON
      detachInterrupt(digitalPinToInterrupt(TARE_BUTTON_PIN));
    #endif
  }
  if (SECONDARY1_BUTTON_PIN > 0) {
    #ifdef TOUCH
      touchAttachInterrupt(SECONDARY1_BUTTON_PIN, touchSecondary1Callback, 0);
    #endif
    #ifdef BUTTON
      detachInterrupt(digitalPinToInterrupt(SECONDARY1_BUTTON_PIN));
    #endif
  }
}


void enableSecondaryButtons() {
  //reattach interrupts
  if (TARE_BUTTON_PIN > 0) {
    #ifdef TOUCH
      touchAttachInterrupt(TARE_BUTTON_PIN, touchTareCallback, TOUCH_TARE_THRESHOLD);
    #endif
    #ifdef BUTTON
      pinMode(TARE_BUTTON_PIN,INPUT_RESISTOR);
      attachInterrupt(digitalPinToInterrupt(TARE_BUTTON_PIN), tareButtonISR, INTERRUPT_MODE);
    #endif
  }
  if (SECONDARY1_BUTTON_PIN > 0) {
    #ifdef TOUCH
      touchAttachInterrupt(SECONDARY1_BUTTON_PIN, touchSecondary1Callback, TOUCH_SECONDARY1_THRESHOLD);
    #endif
    #ifdef BUTTON
      pinMode(SECONDARY1_BUTTON_PIN,INPUT_RESISTOR);
      attachInterrupt(digitalPinToInterrupt(SECONDARY1_BUTTON_PIN), secondary1ButtonISR, INTERRUPT_MODE);
    #endif
  }
}



void attachAllButtonInterrupts() {
  //buttons
  if (POWER_BUTTON_PIN > 0) {
    #ifdef TOUCH
      touchAttachInterrupt(POWER_BUTTON_PIN, touchPowerCallback, TOUCH_POWER_THRESHOLD);
    #endif
    #ifdef BUTTON
      pinMode(POWER_BUTTON_PIN,INPUT_RESISTOR);
      attachInterrupt(digitalPinToInterrupt(POWER_BUTTON_PIN), powerButtonISR, INTERRUPT_MODE);
    #endif
  }
  if (TARE_BUTTON_PIN > 0) {
    #ifdef TOUCH
      touchAttachInterrupt(TARE_BUTTON_PIN, touchTareCallback, TOUCH_TARE_THRESHOLD);
    #endif
    #ifdef BUTTON
      pinMode(TARE_BUTTON_PIN,INPUT_RESISTOR);
      attachInterrupt(digitalPinToInterrupt(TARE_BUTTON_PIN), tareButtonISR, INTERRUPT_MODE);
    #endif
  }
  if (SECONDARY1_BUTTON_PIN > 0) {
    #ifdef TOUCH
      touchAttachInterrupt(SECONDARY1_BUTTON_PIN, touchSecondary1Callback, TOUCH_SECONDARY1_THRESHOLD);
    #endif
    #ifdef BUTTON
      pinMode(SECONDARY1_BUTTON_PIN,INPUT_RESISTOR);
      attachInterrupt(digitalPinToInterrupt(SECONDARY1_BUTTON_PIN), secondary1ButtonISR, INTERRUPT_MODE);
    #endif
  }
}



////////// 2.5.POWER BUTTON LOGIC //////////

void powerButtonPress() {
  DEBUG_PRINTLN("POWER BUTTON PRESS");      
  if (lightSleep || snooze) {
    wakeup = true;
  } else {
    scale.startStopResetTimer();
  }
}

void powerButtonLongPress() {
  DEBUG_PRINTLN("POWER BUTTON LONG PRESS");
  //update maybe ???
  ////check if tare is also touched
  //holdtime = millis() - tareButtonPressedStartTime;
  //if (tareButtonPressing && holdtime > clickThreshold) {
  //  //initiate firmware upgrade
  //  powerButtonPressedStartTime = 0; //reset
  //  DEBUG_PRINTLN("Initiating Upgrade...");
  //  settings.saveSettingByte("otaUpgrade",1);
  //  delay(1000);
  //  ESP.restart();
  //} else {
  powerButtonPressedStartTime = 0; //reset
  deepSleepNow = true;    
  //}
}


//this function called from the main loop, not from the interrupt
void handlePowerButton() {
  static byte powerButtonLEDStatus = 0;

  #ifdef BUTTON
    powerButtonPressing = digitalRead(POWER_BUTTON_PIN);
  #endif

  if (powerButtonPressing && settings.slBtnPress == 1) {
    powerButtonLEDStatus = 1;
    toggleStatusLed(true);
  } else if (powerButtonLEDStatus == 1) {
    powerButtonLEDStatus = 0;
    toggleStatusLed(false);
  }

    
  if (powerButtonPressedStartTime>0) {
    uint32_t holdtime = millis() - powerButtonPressedStartTime;
    if (!powerButtonPressing) {
      if (holdtime > clickThreshold) {
        powerButtonPress();
        if (powerButtonLEDStatus == 1) {
          powerButtonLEDStatus = 0;
          toggleStatusLed(false);
        }
      }
      powerButtonPressedStartTime = 0; //reset
    } else {
      //detect long/special touch even before releasing
      if (holdtime > lClickThr) {
        powerButtonLongPress();   
        powerButtonPressedStartTime = 0; //reset
        if (powerButtonLEDStatus == 1) {
          powerButtonLEDStatus = 0;
          toggleStatusLed(false);
        }
      }
    }   
    lastActionMillis = millis();
  }
  #ifdef TOUCH
    powerButtonPressing = false; //always reset
  #endif
}






////////// 2.6.TARE BUTTON LOGIC //////////
void tareButtonPress() {
  DEBUG_PRINTLN("TARE BUTTON PRESS");       
  //delay some ms to avoid reading the touch force
  //set variables and let main loop do the actual tare
  beginTare = millis() + tareButtonDelay;
  tareType = 0;
  calibrateTare = false;
  saveWeight = true;  
  if (lightSleep || snooze) {
    wakeup = true;
  }
}


void tareButtonLongPress() {
  //do a tare + calibration
  //set variables and let main loop do the actual tare
  DEBUG_PRINTLN("TARE BUTTON LONG PRESS");   
  
  if (settings.slBtnPress == 1) {
    for (uint8_t i=0;i<2;i++) {
      toggleStatusLed();
      delay(500);
    }
    toggleStatusLed(false);
  }
  
  beginTare = millis() + tareButtonDelay;
  tareType = 2;
  calibrateTare = true;
  saveWeight = true;        
  if (lightSleep || snooze) {
    wakeup = true;
  }
}



void handleTareButton() {   
  //this function called from the main loop, not from the interrupt 
  static byte tareButtonLEDStatus = 0;
  if (tareButtonPressedStartTime>0) {
    #ifdef BUTTON
      tareButtonPressing = digitalRead(TARE_BUTTON_PIN);
    #endif

    
    if (tareButtonPressing && settings.slBtnPress == 1) {
      tareButtonLEDStatus = 1;
      toggleStatusLed(true);
    } else if (tareButtonLEDStatus == 1) {      
      toggleStatusLed(false);
    }
    
    uint32_t holdtime = millis() - tareButtonPressedStartTime;
    if (!tareButtonPressing) {
      if (holdtime > clickThreshold) {
        if (tareButtonLEDStatus == 1) {
          toggleStatusLed(false);
        }
        tareButtonPress();
      }
      tareButtonPressedStartTime = 0; //reset
    } else {
      if (holdtime > lClickThr) {
        tareButtonLongPress();
        tareButtonPressedStartTime = 0; //reset
      }
    }   
    lastActionMillis = millis();
  }
  
  #ifdef TOUCH
    tareButtonPressing = false; //always reset
  #endif
}




////////// 2.7.SECONDARY BUTTON LOGIC //////////
void handleSecondary1Button() {
  //this function called from the main loop, not from the interrupt
  static byte secondary1ButtonLEDStatus = 0;
  #ifdef BUTTON
    secondary1ButtonPressing = digitalRead(SECONDARY1_BUTTON_PIN);
  #endif
  
  if (secondary1ButtonPressing && settings.slBtnPress == 1) {
    secondary1ButtonLEDStatus = 1;
    toggleStatusLed(true);
  } else if (secondary1ButtonLEDStatus == 1) {
    toggleStatusLed(false);
  }
  
  if (secondary1ButtonPressedStartTime>0) {
    uint32_t holdtime = millis() - secondary1ButtonPressedStartTime;
    if (!secondary1ButtonPressing) {
      //previously true, now false
      if (holdtime > clickThreshold) {
        DEBUG_PRINTLN("Touch of secondary1 button");    
        if (lightSleep || snooze) {
          wakeup = true;
        }
        if (secondary1ButtonLEDStatus == 1) {
          toggleStatusLed(false);
        }
      }
      secondary1ButtonPressedStartTime = 0; //reset
    } else {
      if (holdtime > lClickThr) {
        secondary1ButtonPressedStartTime = 0; //reset
        if (secondary1ButtonLEDStatus == 1) {
          toggleStatusLed(false);
        }
        DEBUG_PRINTLN("Long touch of secondary1 button");
      }
    }   
    lastActionMillis = millis();
  }
  #ifdef TOUCH
    secondary1ButtonPressing = false; //always reset
  #endif
  
}
