/*  
  ESPresso Scale
  Created by John Sartzetakis, Jan 2019
  Released into the public domain.
  https://gitlab.com/jousis/espresso-scale
*/

#ifndef SETTINGS_h
#define SETTINGS_h

#include <Preferences.h>

class SETTINGS
{

  public:
    SETTINGS();
    uint8_t otaUpgrade = 0; //OTA is enabled by long touching T8 & T9 during startup
    
    uint8_t bleEnabled = DEFAULT_BLE_ENABLED;

    uint8_t snoozeTimeout = DEFAULT_SNOOZE_TIMEOUT;
    uint8_t lSlpTimeout = DEFAULT_LIGHT_SLEEP_TIMEOUT;
    uint8_t dSlpTimeout = DEFAULT_DEEP_SLEEP_TIMEOUT;
    uint8_t autoTare = DEFAULT_AUTO_TARE;
    uint8_t aTareNeg = DEFAULT_AUTO_TARE_NEGATIVE;
    uint8_t autoStartTimer = DEFAULT_AUTO_START_TIMER;
    uint8_t timerDelay = DEFAULT_TIMER_DELAY;
    uint8_t tmrStartW = DEFAULT_TIMER_START_WEIGHT;
    uint8_t timerStopWeight = DEFAULT_TIMER_STOP_WEIGHT;
    uint8_t rocStartTimer = DEFAULT_ROC_START_TIMER;
    uint8_t rocStopTimer = DEFAULT_ROC_STOP_TIMER;
    uint8_t tmrShowNeg = DEFAULT_TIMER_SHOW_NEGATIVE;
    uint8_t autoStopTimer = DEFAULT_AUTO_STOP_TIMER;
    uint8_t scaleUnits = DEFAULT_SCALE_UNITS;
    uint8_t zeroTracking = DEFAULT_ZERO_TRACKING;
    uint8_t stWDiff = DEFAULT_STABLE_WEIGHT_DIFF;
    uint8_t zeroRange = DEFAULT_ZERO_RANGE;
    uint8_t fakeRange = DEFAULT_FAKE_STABILITY_RANGE;
    uint8_t sensitivity = DEFAULT_SENSITIVITY;
    uint8_t smoothing = DEFAULT_SMOOTHING;
    uint8_t adcSpeed = DEFAULT_ADC_SPEED;
    uint8_t decimalDigits = DEFAULT_DECIMAL_DIGITS;
    uint8_t readSamples = DEFAULT_READ_SAMPLES;
    uint8_t slBtnPress = DEFAULT_STATUS_LED_BUTTON_PRESS;
    uint8_t slMaxVIN = DEFAULT_STATUS_LED_MAX_VIN;
    uint8_t batReadInterval = DEFAULT_BATTERY_LEVEL_READ_INTERVAL;
    uint8_t displayRotation = DEFAULT_DISPLAY_ROTATION;    
    uint8_t displayMaxBr = DEFAULT_DISPLAY_MAX_BRIGHTNESS;
    uint8_t displayFps = DEFAULT_DISPLAY_FPS;
    uint8_t displayTopH = DEFAULT_DISPLAY_TOP_H;
    uint8_t displayMainH = DEFAULT_DISPLAY_MAIN_H;
    uint8_t displayBottomH = DEFAULT_DISPLAY_BOTTOM_H;
    uint8_t bleNps = DEFAULT_BLE_NPS;
    uint8_t graphInSection = DEFAULT_GRAPH_IN_SECTION;
    uint8_t hrMode = DEFAULT_HR_MODE;
    uint8_t hrAutoSwSpd = DEFAULT_HR_AUTOSWITCH_SPEED;
    uint8_t hrPeriod = DEFAULT_HR_PERIOD;
    
    uint16_t tareButtonDelay = DEFAULT_TARE_DELAY;
    
    uint32_t calFactorULong = DEFAULT_CAL_FACTOR_ULONG;
    uint32_t colorMain = DEFAULT_COLOR_MAIN;
    uint32_t colorTop = DEFAULT_COLOR_TOP;
    uint32_t colorBottom = DEFAULT_COLOR_BOTTOM;
    uint32_t colorMainBg = DEFAULT_COLOR_MAIN_BG;
    uint32_t colorTopBg = DEFAULT_COLOR_TOP_BG;
    uint32_t colorBottomBg = DEFAULT_COLOR_BOTTOM_BG;        
    uint32_t clickThreshold = DEFAULT_BUTTON_CLICK_THRESHOLD;
    uint32_t lClickThr = DEFAULT_BUTTON_LONGCLICK_THRESHOLD;

    
    String wifiHostname = ""; //check out the default values at resetSettings()
    String wifiSSID = "";
    String wifiPassword = "";
    
    String bootTextTop = "";
    String bootTextMiddle = "";
    String bootTextBottom = "";
    String zeroTrackingStr = DEFAULT_ZERO_TRACKING_STR;
    
    void clearEEPROM();
    void resetSettings();
    bool loadSettings();
    void saveSettingByte(const char* key, uint8_t value);
    void saveSettingUShort(const char* key, uint16_t value);
    void saveSettingULong(const char* key, uint32_t value);    
    void saveSettingString(const char* key, String value);
    void saveSettings();
  
  protected:
    Preferences preferences;
    //If you change anything in the EEPROM save function change the revision/...
    //and alter the reading function accordingly (optional)
    const uint8_t EEPROM_STRUCT_VERSION_MAJOR = 0;
    const uint8_t EEPROM_STRUCT_VERSION_MINOR = 0;
    const uint8_t EEPROM_STRUCT_VERSION_REVISION = 1;
    
    ///// DEFAULT SETTINGS /////
    const uint8_t DEFAULT_SNOOZE_TIMEOUT = 0; //3 = 30seconds, 0=disabled, in 10*seconds, dims the display, inserts 250ms delay in the loop=> approx 4 ADC reads per second
    const uint8_t DEFAULT_LIGHT_SLEEP_TIMEOUT = 0; //6 = 1min, 0=disabled, in 10*seconds, shuts down ADC using its power pin (not the LDO) and dims the display
    const uint8_t DEFAULT_DEEP_SLEEP_TIMEOUT = 0; //12 = 2mins, 0=disabled, in 10*seconds, shuts down ADC LDO,display and puts ESP32 to deep sleep. Consumes very little power. This is essentialy our power off mode.
    const uint8_t DEFAULT_AUTO_TARE=0;
    const uint8_t DEFAULT_AUTO_TARE_NEGATIVE=0;
    const uint8_t DEFAULT_AUTO_START_TIMER=0;
    const uint8_t DEFAULT_TIMER_DELAY=0;
    const uint8_t DEFAULT_TIMER_START_WEIGHT=15; //1.5gr
    const uint8_t DEFAULT_TIMER_STOP_WEIGHT=8; //8 grams
    const uint8_t DEFAULT_ROC_START_TIMER = 5; // 0.5gr/s
    const uint8_t DEFAULT_ROC_STOP_TIMER = 15; // 1.5gr/s
    const uint8_t DEFAULT_TIMER_SHOW_NEGATIVE=0;
    const uint8_t DEFAULT_AUTO_STOP_TIMER=0;
    const uint8_t DEFAULT_SCALE_UNITS = 0; //0=grams , nothing else is implemented for now
    const uint8_t DEFAULT_ZERO_TRACKING = 10; //in centigrams, 0=disabled
    const uint8_t DEFAULT_SENSITIVITY = 255; //1 = minimum, best for 80SPS, 255 = max, best for 10SPS
    const uint8_t DEFAULT_SMOOTHING = 1; //1 = enabled,0=disabled
    const uint8_t DEFAULT_ADC_SPEED = 10; //10 or 80
    const uint8_t DEFAULT_DECIMAL_DIGITS = 1; //the final rounding before showing to the display
    const uint8_t DEFAULT_BLE_ENABLED = 1;
    const uint8_t DEFAULT_READ_SAMPLES = 1; //how many samples per read
    const uint8_t DEFAULT_STATUS_LED_BUTTON_PRESS = 1; //switch on led on button press
    const uint8_t DEFAULT_STATUS_LED_MAX_VIN = 0; //keep led on when analog VIN >4.3V
    const uint8_t DEFAULT_BATTERY_LEVEL_READ_INTERVAL = 0; //in seconds, not in snooze/standby/deep sleep. By default battery level is only read on startup and if mobile app asks.
    const uint8_t DEFAULT_DISPLAY_ROTATION = 0; //0-3 usually but check your library
    const uint8_t DEFAULT_DISPLAY_FPS = 10; //10 fps
    const uint8_t DEFAULT_DISPLAY_TOP_H = 16;
    const uint8_t DEFAULT_DISPLAY_MAIN_H = 16;
    const uint8_t DEFAULT_DISPLAY_BOTTOM_H = 0;
    const uint8_t DEFAULT_BLE_NPS = 3; //how many updates per second for NOTIFY characteristic
    const uint8_t DEFAULT_GRAPH_IN_SECTION = 0;
    #ifdef LEDSEGMENT
      const uint8_t DEFAULT_DISPLAY_MAX_BRIGHTNESS = 15;
    #else
      const uint8_t DEFAULT_DISPLAY_MAX_BRIGHTNESS = 255;
    #endif
    const uint8_t DEFAULT_STABLE_WEIGHT_DIFF = 5; //in centigrams, 5 is ok for 10SPS and 80 with filtering if using a good load cell
    const uint8_t DEFAULT_ZERO_RANGE = 0; //in centigrams
    const uint8_t DEFAULT_FAKE_STABILITY_RANGE = 0; //in centigrams
    const uint8_t DEFAULT_HR_MODE = 0;
    const uint8_t DEFAULT_HR_AUTOSWITCH_SPEED = 1;
    const uint32_t DEFAULT_HR_PERIOD = 10; //10s

    const uint16_t DEFAULT_TARE_DELAY = 750;
    
    const uint32_t DEFAULT_CAL_FACTOR_ULONG = 14000; //actual calfactor you want for your load cell * 10.
    const uint32_t DEFAULT_COLOR_MAIN = 4294967295; //WHITE
    const uint32_t DEFAULT_COLOR_TOP = 4294967040; //YELLOW
    const uint32_t DEFAULT_COLOR_BOTTOM = 4294967295; //WHITE
    const uint32_t DEFAULT_COLOR_MAIN_BG = 4278190080; //BLACK
    const uint32_t DEFAULT_COLOR_TOP_BG = 4278190080; //BLACK
    const uint32_t DEFAULT_COLOR_BOTTOM_BG = 4278190080; //BLACK
    const uint32_t DEFAULT_BUTTON_CLICK_THRESHOLD = 10;
    const uint32_t DEFAULT_BUTTON_LONGCLICK_THRESHOLD = 3000;
    
    const String DEFAULT_WIFI_HOSTNAME = "OSES";
    const String DEFAULT_WIFI_SSID = "oses";
    const String DEFAULT_WIFI_PASSWORD = "oses";
    const String DEFAULT_ZERO_TRACKING_STR = "0.1"; //in grams, 0=disabled
    
    
};

#endif
